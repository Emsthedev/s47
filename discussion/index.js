// console.log("Test");

//quereSelector() is a method that can be used to select a specific element
//from our document
console.log(document.querySelector("#txt-first-name"));
/* document refers to the whole content of the page */
console.log(document);

console.log(document.querySelector("#txt-last-name"));
/* Alternative mothods that we can use aside from query selector in retrieving elements
    Syntax:
        document.getElementById()
        document.getElementByClassName()
        document.getElementByTagName()

*/

// DOM Manipulation

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");
const spanLastName = document.querySelector("#span-last-name");
console.log(txtFirstName);
console.log(spanFullName);

/* 
        Event:
            click, hover, keypress, keyup and etc.
    
        Event Listeners:
            Allows us to let our users interact with our page. Each click or hover
            is an event which can trigger a function or task.
        
        Syntax:

            selectedElement.addEventListener('event in string form', function);




*/

/* 1st way txtFirstName.addEventListener('keyup', (event)=> {
    spanFullName.innerHTML = txtFirstName.value 
}); */

txtFirstName.addEventListener('keyup', printFirstName);
function printFirstName(event) {
    spanFullName.innerHTML = txtFirstName.value 
};
txtLastName.addEventListener('keyup', (event)=> {
    spanLastName.innerHTML = txtLastName.value 
});





/* 
    What is innerHTML
    - is a property of an element which considers all 
    the children of the selected element as a string.

    .value of the input text field

*/
txtFirstName.addEventListener('keyup', (event)=> {
    console.log(event)
    console.log(event.target)
    console.log(event.target.value)
});

const labelFirstName = document.querySelector("#labeled-text-name");
labelFirstName.addEventListener('click', (event)=> {
        console.log(event)
        alert("You clicked the First Name Label.")
});



